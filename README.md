
```
==========================================================================
    __    _____    ___   __                   __    __                __  
   / /   /  _/ |  / / | / /    _      _____  / /_  / /_  ____  ____  / /__
  / /    / / | | / /  |/ /____| | _  / / _ \/ __ \/ __ \/ __ \/ __ \/ //_/
 / /____/ /  | |/ / /|  /_____/ |/ |/ /  __/ /_/ / / / / /_/ / /_/ / . (   
/_____/___/  |___/_/ |_/      |__/|__/\___/_.___/_/ /_/\____/\____/_/|_|  
 
==============   ~ SpringBoot ~ Lombok ~ JSON Web Token ~   ==============
```

# LIVN Webhook

Sample implementation of webhook receiving endpoint.

### Webhook Endpoint registration

First step is to get your webhook online. Register you webhook with request

```
POST https://LIVN-SATELLITE-URL/livngds-2016/api/webhook
Content-Type: application/json
Authorization: Basic cm1**********MDE=
{
	"webhooks": {
		"bookingStatusChange": "YOUR-WEBHOOK-URL",
		"apiKey": "API-KEY",
		"secretKey": "SECRET-KEY",
		"checkPhrase": "CHECK-PHRASE"
}
```

Where:

| Request parameter  |         |
| :----------------- | :------ |
| LIVN-SATELLITE-URL | URL of satellite you will be using for booking |
| YOUR-WEBHOOK-URL   | webhook URL where LIVN will send notifications about booking status change |
| API-KEY            | key which LIVN will add to every request header with key ``"X-api-key"`` |
| SECRET-KEY         | secret key wich LIVN will use to create ``JSON Web Token`` |
| CHECK-PHRASE       | Registration request only. Upon webhook request registration LIVN will ping URL registered. This check phrase MUST BE send back in the response header with header key ``"X-check-phrase"`` |

Response will be like that:

```javascript
{
    "webhooks": {
        "bookingStatusChange": "YOUR_WEBHOOK_URL",
        "apiKey": "API-KEY",
        "secretKey": "S********Y",
        "checkPhrase": "CHECK-PHRASE"
    }
}
```

After successful registration your webhook is ready to accept calls 

### Test webhook app

```
POST localhost:8081/livn/webhook
Content-Type: application/json
X-api-key: api-key
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJvbGRTdGF0dXMiOiJDT05GSVJNRURfV0FJVElOR19BUFBST1ZBTCIsImJvb2tpbmdSZWZlcmVuY2UiOiIxMDA1MDAtMTI1MCIsIm5ld1N0YXR1cyI6IkNPTkZJUk1FRCIsInRpbWVzdGFtcCI6IjIwMTktMDQtMThUMDk6NDU6NTIuNDk5KzEwMDAifQ.xK5FV2gSU9Dt4weBf-RYwGewA86-qJv83MwPKIxyIXQ
{
    "bookingReference": "100500-1250",
    "newStatus": "CONFIRMED",
    "oldStatus": "CONFIRMED_WAITING_APPROVAL",
    "timestamp": "2019-04-18T09:45:52.499+1000"
}
```

Response:

```javascript
{
    "status": "ok",
    "message": null,
    "timestamp": 1555988247264
}
```

### Resources
 
 * [JSON Web Token](https://jwt.io/)
 * [Java implementation of JSON Web Token (JWT)](https://github.com/auth0/java-jwt)