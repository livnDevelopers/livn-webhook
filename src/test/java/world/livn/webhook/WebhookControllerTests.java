package world.livn.webhook;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WebhookControllerTests {


    @Value("${api.key}")
    private String API_KEY;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldPing() throws Exception {

        this.mockMvc.perform(get("/webhook/ping")
                  .accept(MediaType.APPLICATION_JSON))
                  .andDo(print())
                  .andExpect(status().isOk())
                  .andExpect(jsonPath("$.status").value("ok"));
    }

    @Test
    public void shouldExecuteWebhook() throws Exception {

        final String CHECK_PHRASE = "check-phrase";
        
        this.mockMvc.perform(post("/webhook")
                .header(LivnResponse.HEADER_CHECK_PHRASE, CHECK_PHRASE)
                .header(LivnResponse.HEADER_X_API_KEY, this.API_KEY)
                .content("{\"ok\":true}")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string(LivnResponse.HEADER_CHECK_PHRASE, CHECK_PHRASE));
    }

    @Test
    public void shouldValidateWebToken() throws Exception {

        StringBuilder jsonContent = new StringBuilder();
        jsonContent.append("{");
        for (String[] attr: JsonWebTokenTest.BODY) {
            jsonContent.append("\"" + attr[0] + "\":\"" + attr[1] + "\",");
        }
        jsonContent.append("\"last\":\"last\"}");
        
        this.mockMvc.perform(post("/webhook")
                .header(HttpHeaders.AUTHORIZATION, "Bearer " + JsonWebTokenTest.JWToken)
                .header(LivnResponse.HEADER_X_API_KEY, this.API_KEY)
                .content(jsonContent.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
