package world.livn.webhook;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Arrays;

import org.junit.Test;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator.Builder;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;


/**
 * https://jwt.io/#debugger-io?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJib29raW5nUmVmZXJlbmNlIjoiMTAwNTAwLTEyMTIiLCJuZXdTdGF0dXMiOiJDT05GSVJNRUQiLCJvbGRTdGF0dXMiOiJDT05GSVJNRURfV0FJVElOR19BUFBST1ZBTCIsInRpbWVzdGFtcCI6IjIwMTktMDQtMThUMDk6NDU6NTIuNDk5KzEwMDAifQ.9duIfcXY0Oyud9VbYLLAUi7OwBjKqJCuYQyGrGr7fSk
 */
public class JsonWebTokenTest {

    final static String[][] BODY = new String[][] {
            new String[] {"bookingReference", "100500-1212"},
            new String[] {"oldStatus", "CONFIRMED_WAITING_APPROVAL"},
            new String[] {"newStatus", "CONFIRMED"},
            new String[] {"timestamp", "2019-04-18T09:45:52.499+1000"},
    }; 
    final static String SECRET = "hmac-256-bit-secret"; 
    final static String JWToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJib29raW5nUmVmZXJlbmNlIjoiMTAwNTAwLTEyMTIiLCJuZXdTdGF0dXMiOiJDT05GSVJNRUQiLCJvbGRTdGF0dXMiOiJDT05GSVJNRURfV0FJVElOR19BUFBST1ZBTCIsInRpbWVzdGFtcCI6IjIwMTktMDQtMThUMDk6NDU6NTIuNDk5KzEwMDAifQ.9duIfcXY0Oyud9VbYLLAUi7OwBjKqJCuYQyGrGr7fSk";
    
    @Test
    public void shouldCreateValidSignature() throws Exception {
        Algorithm algorithmHS = Algorithm.HMAC256(SECRET);
        Builder builder = JWT.create();
        for (String[] entry: BODY) {
            builder = builder.withClaim(entry[0], entry[1]);
        }
        String signed = builder.sign(algorithmHS);
        assertThat(signed, is(JWToken));
    }
    
    @Test
    public void shouldReadClaims() {
        DecodedJWT jwt = JWT.decode(JWToken);
        Arrays.stream(BODY).forEach(entry -> {
            assertThat(jwt.getClaim(entry[0]).asString(), is(entry[1]));
        });
    }
    
    @Test
    public void shouldVerifyJWToken() throws Exception {
        DecodedJWT jwt = JWT.require(Algorithm.HMAC256(SECRET)).build().verify(JWToken);
        assertThat(jwt, is(notNullValue()));
    }
    
    @Test(expected = SignatureVerificationException.class)
    public void shouldNotVerifyJWToken() throws Exception {
        JWT.require(Algorithm.HMAC256(SECRET)).build().verify(JWToken + "salt");
    }
    
}
