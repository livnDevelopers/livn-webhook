package world.livn.webhook;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Getter
@Builder
public class LivnResponse {

    public final static String HEADER_X_API_KEY = "X-api-key";
    public final static String HEADER_CHECK_PHRASE = "X-check-phrase";

    public static enum Status { 
        ok,
        error
    };
    
    @NonNull
    private Status status;
    private String message;
    @Builder.Default
    private long timestamp = System.currentTimeMillis();
}
