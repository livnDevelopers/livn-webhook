package world.livn.webhook;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames=true)
public class WebhookNotificationMessage {
	public String bookingReference;
	public String oldStatus;
	public String newStatus;
	// "2019-04-18T09:45:52.499+1000"
	public String timestamp;

}
