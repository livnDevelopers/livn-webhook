package world.livn.webhook;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import lombok.extern.slf4j.Slf4j;

@RestController("webhook")
@Slf4j
public class WebhookController {

    @Value("${api.key}")
    private String API_KEY;
    
    @Value("${hmac.secret.key}")
    private String HMAC_SECRET_KEY;
    
    @RequestMapping(method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<LivnResponse> ping() {
        log.info("ping");
        return ResponseEntity.status(HttpStatus.OK).body(LivnResponse.builder().status(LivnResponse.Status.ok).message("ping").build());
    }

    @RequestMapping(path="error500", method={RequestMethod.GET,RequestMethod.POST}, produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<LivnResponse> error500() {
        log.info("error 500");
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(LivnResponse.builder().status(LivnResponse.Status.error).message("generated error 500").build());
    }

    @RequestMapping(method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<LivnResponse> echo(
            @RequestHeader(value=HttpHeaders.AUTHORIZATION, required=false) String authorisation,
            @RequestHeader(value=LivnResponse.HEADER_X_API_KEY, required=false) String apiKey,
            @RequestHeader(value=LivnResponse.HEADER_CHECK_PHRASE, required=false) String checkPhrase,
            @RequestBody(required=false) WebhookNotificationMessage payload) {

        log.info("echo received request with header [api-key:'{}';check-phrase:'{}';authorisation:'{}'] payload [{}]", apiKey, checkPhrase, authorisation, payload);

        try {
            if (Objects.equals(apiKey, this.API_KEY)) {
                log.info("API key verified ...");
            } else {
                log.info("API key not verified ...");
                return ResponseEntity.
                        status(HttpStatus.BAD_REQUEST).
                        body(LivnResponse.builder().status(LivnResponse.Status.error).message("wrong API key").build());
            }
            if (StringUtils.isNotBlank(authorisation)) {
                DecodedJWT jwt = JWT.decode(authorisation.replace("Bearer ", ""));
                try {
                    JWT.require(Algorithm.HMAC256(this.HMAC_SECRET_KEY)).build().verify(jwt);
                    log.info("signature verified ...");
                } catch (JWTVerificationException exception) {
                    log.info("signature not verified ...");
                    return ResponseEntity.
                            status(HttpStatus.BAD_REQUEST).
                            body(LivnResponse.builder().status(LivnResponse.Status.error).message("wrong signature").build());
                }
                if (Objects.equals(payload.getBookingReference(), jwt.getClaim("bookingReference").asString()) &&
                        Objects.equals(payload.getOldStatus(), jwt.getClaim("oldStatus").asString()) &&
                        Objects.equals(payload.getNewStatus(), jwt.getClaim("newStatus").asString()) &&
                        Objects.equals(payload.getTimestamp(), jwt.getClaim("timestamp").asString())) {
                    log.info("claims approved ...");
                } else {
                    log.info("claims are not approved ...");
                    return ResponseEntity.
                            status(HttpStatus.BAD_REQUEST).
                            body(LivnResponse.builder().status(LivnResponse.Status.error).message("claims are not approved").build());
                }
            } else {
                log.info("signature not provided ...");
            }
        } catch (Exception exc) {
            return ResponseEntity.
                    status(HttpStatus.INTERNAL_SERVER_ERROR).
                    body(LivnResponse.builder().status(LivnResponse.Status.error).message(exc.getMessage()).build());
        }
        
        BodyBuilder responseBuilder = ResponseEntity.status(HttpStatus.OK);
        if (StringUtils.isNotBlank(checkPhrase)) {
            responseBuilder = responseBuilder.header(LivnResponse.HEADER_CHECK_PHRASE, checkPhrase);
        }
        return responseBuilder.body(LivnResponse.builder().status(LivnResponse.Status.ok).build());
    }

    @RequestMapping(path="noHeader", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<LivnResponse> echoNoHeader() {
        log.info("echo received request for no-header response");
        return ResponseEntity.
                status(HttpStatus.OK).
                body(LivnResponse.builder().status(LivnResponse.Status.ok).message("no header").build());
    }

}
